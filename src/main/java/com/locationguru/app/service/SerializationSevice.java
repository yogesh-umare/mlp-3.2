package com.locationguru.app.service;

import com.locationguru.app.model.init.ServiceInit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

@Service
public class SerializationSevice
{
	private static final Logger logger = LogManager.getLogger(SerializationSevice.class);

	public ServiceInit deserialize(String request)
	{
		ServiceInit serviceInit = null;
		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(ServiceInit.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			serviceInit = (ServiceInit) jaxbUnmarshaller.unmarshal(new StringReader(request));
		}
		catch (JAXBException e)
		{
			logger.error(e.getMessage());
		}
		return serviceInit;
	}

}
