package com.locationguru.app.model.init;

import com.locationguru.app.model.common.Header;
import com.locationguru.app.model.common.RequestVersion;
import com.locationguru.app.model.slir.SLIRequest;
import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.SERVICE_INIT_TAG)
public class ServiceInit extends RequestVersion
{
	@XmlElement(name =  MLPConstants.HEADER_TAG)
	private Header header;

	@XmlElement(name =  MLPConstants.SLIREQUEST_TAG)
	private SLIRequest sliRequest;

	public Header getHeader()
	{
		return header;
	}

	public void setHeader(final Header header)
	{
		this.header = header;
	}

	public SLIRequest getSliRequest()
	{
		return sliRequest;
	}

	public void setSliRequest(final SLIRequest sliRequest)
	{
		this.sliRequest = sliRequest;
	}

}
