package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.VMSCID_TAG)
public class VmsCellId
{
	@XmlElement(name = MLPConstants.CC_TAG)
	private CountryCode countryCode;

	@XmlElement(name = MLPConstants.NDC_TAG)
	private NationDesignationCode designationCodeVO;

	private String vmscno;

	public CountryCode getCountryCode()
	{
		return countryCode;
	}

	public void setCountryCode(final CountryCode countryCode)
	{
		this.countryCode = countryCode;
	}

	public NationDesignationCode getDesignationCodeVO()
	{
		return designationCodeVO;
	}

	public void setDesignationCodeVO(final NationDesignationCode designationCodeVO)
	{
		this.designationCodeVO = designationCodeVO;
	}

	public String getVmscno()
	{
		return vmscno;
	}

	public void setVmscno(final String vmscno)
	{
		this.vmscno = vmscno;
	}
}
