package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class HorizontalAccuracy
{
	@XmlAttribute(name =  MLPConstants.QOS_CLASS_ATTR)
	private String qosClass;

	@XmlValue
	private String value;

	public String getQosClass()
	{
		return qosClass;
	}

	public void setQosClass(final String qosClass)
	{
		this.qosClass = qosClass;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}
}
