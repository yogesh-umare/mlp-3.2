package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.COORDINATE_REFERENCE_SYSTEM)
public class CoordinateReferenceSystem
{
	@XmlElement(name =  MLPConstants.IDENTIFIER)
	private Identifier identifier;

	public Identifier getIdentifier()
	{
		return identifier;
	}

	public void setIdentifier(final Identifier identifier)
	{
		this.identifier = identifier;
	}
}
