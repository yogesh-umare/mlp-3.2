package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import com.locationguru.app.enumerations.Enums.LocationTypeConstant;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.LOCATION_TYPE_TAG)
public class LocationType
{
	@XmlAttribute
	private String type = LocationTypeConstant.CURRENT.name();

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}
}
