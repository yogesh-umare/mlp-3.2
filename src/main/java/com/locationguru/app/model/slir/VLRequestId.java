package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.VLRID_TAG)
public class VLRequestId
{
	@XmlElement(name =  MLPConstants.CC_TAG)
	private CountryCode countryCode;

	@XmlElement(name =  MLPConstants.NDC_TAG)
	private NationDesignationCode designationCodeVO;

	private String vlrno;

	public CountryCode getCountryCode()
	{
		return countryCode;
	}

	public NationDesignationCode getDesignationCodeVO()
	{
		return designationCodeVO;
	}

	public void setDesignationCodeVO(final NationDesignationCode designationCodeVO)
	{
		this.designationCodeVO = designationCodeVO;
	}

	public String getVlrno()
	{
		return vlrno;
	}

	public void setVlrno(final String vlrno)
	{
		this.vlrno = vlrno;
	}

	public void setCountryCode(final CountryCode countryCode)
	{
		this.countryCode = countryCode;
	}
}
