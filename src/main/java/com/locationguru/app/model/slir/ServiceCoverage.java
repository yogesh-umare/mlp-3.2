package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.SERVICE_COVERAGE_TAG)
public class ServiceCoverage
{
	@XmlElement(name =  MLPConstants.CC_TAG)
	private Set<CountryCode> countryCodes = new HashSet<>();

	@XmlElement(name =  MLPConstants.NDC_TAG)
	private Set<NationDesignationCode> nationalDestinationCodes;

	public Set<CountryCode> getCountryCodes()
	{
		return countryCodes;
	}

	public void setCountryCodes(final Set<CountryCode> countryCodes)
	{
		this.countryCodes = countryCodes;
	}

	public Set<NationDesignationCode> getNationalDestinationCodes()
	{
		return nationalDestinationCodes;
	}

	public void setNationalDestinationCodes(final Set<NationDesignationCode> nationalDestinationCodes)
	{
		this.nationalDestinationCodes = nationalDestinationCodes;
	}
}
