package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import com.locationguru.app.enumerations.Enums.PriorityConstant;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.PRIORITY_TAG)
public class Priority
{
	@XmlAttribute
	PriorityConstant type = PriorityConstant.NORMAL;

	public PriorityConstant getType()
	{
		return type;
	}

	public void setType(final PriorityConstant type)
	{
		this.type = type;
	}
}
