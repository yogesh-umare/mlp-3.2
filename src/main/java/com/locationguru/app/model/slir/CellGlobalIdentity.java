package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.CGI_TAG)
public class CellGlobalIdentity
{
	@XmlElement(name = MLPConstants.MCC_TAG)
	private String mobileCountryCode;

	@XmlElement(name = MLPConstants.MNC_TAG)
	private String mobileNetworkCode;

	@XmlElement(name = MLPConstants.LAC_TAG)
	private String localAreaCode;

	@XmlElement(name = MLPConstants.CELLID_TAG)
	private String cellId;

	public String getMobileCountryCode()
	{
		return mobileCountryCode;
	}

	public void setMobileCountryCode(final String mobileCountryCode)
	{
		this.mobileCountryCode = mobileCountryCode;
	}

	public String getMobileNetworkCode()
	{
		return mobileNetworkCode;
	}

	public void setMobileNetworkCode(final String mobileNetworkCode)
	{
		this.mobileNetworkCode = mobileNetworkCode;
	}

	public String getLocalAreaCode()
	{
		return localAreaCode;
	}

	public void setLocalAreaCode(final String localAreaCode)
	{
		this.localAreaCode = localAreaCode;
	}

	public String getCellId()
	{
		return cellId;
	}

	public void setCellId(final String cellId)
	{
		this.cellId = cellId;
	}
}
