package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import com.locationguru.app.enumerations.Enums;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.MSID_TAG)
public class Msid
{
	@XmlAttribute
	private Enums.MSIDType type = Enums.MSIDType.MSISDN;

	@XmlValue
	private String value;

	@XmlAttribute(name = MLPConstants.ENCODING)
	private Enums.Encoding encoding = Enums.Encoding.ASC;

	public Enums.MSIDType getType()
	{
		return type;
	}

	public void setType(final Enums.MSIDType type)
	{
		this.type = type;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public Enums.Encoding getEncoding()
	{
		return encoding;
	}

	public void setEncoding(final Enums.Encoding encoding)
	{
		this.encoding = encoding;
	}
}
