package com.locationguru.app.model.slir;

import com.locationguru.app.enumerations.Enums.ResponseTimeType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseRequired
{
	@XmlAttribute
	private ResponseTimeType type = ResponseTimeType.DELAY_TOL;

	public ResponseTimeType getType()
	{
		return type;
	}

	public void setType(final ResponseTimeType type)
	{
		this.type = type;
	}
}
