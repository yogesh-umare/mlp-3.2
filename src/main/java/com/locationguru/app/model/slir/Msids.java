package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.MSIDS_TAG)
public class Msids
{

	@XmlElement(name =  MLPConstants.MSID_TAG)
	private List<Msid> msid = new ArrayList<>();

	@XmlElement(name =  MLPConstants.MSID_RANGE_TAG)
	private List<MsidRange> msidRange = new ArrayList<>();

	@XmlElement(name =  MLPConstants.CODEWORD_TAG)
	private List<Codeword> codewords = new ArrayList<>();

	private List<Session> session = new ArrayList<>();

	@XmlElement(name =  MLPConstants.TRANS_ID_TAG)
	private List<TransmissionId> transmissionIds = new ArrayList<>();

	public List<Msid> getMsid()
	{
		return msid;
	}

	public void setMsid(final List<Msid> msid)
	{
		this.msid = msid;
	}

	public List<MsidRange> getMsidRange()
	{
		return msidRange;
	}

	public void setMsidRange(final List<MsidRange> msidRange)
	{
		this.msidRange = msidRange;
	}

	public List<Codeword> getCodewords()
	{
		return codewords;
	}

	public void setCodewords(final List<Codeword> codewords)
	{
		this.codewords = codewords;
	}

	public List<Session> getSession()
	{
		return session;
	}

	public void setSession(final List<Session> session)
	{
		this.session = session;
	}

	public List<TransmissionId> getTransmissionIds()
	{
		return transmissionIds;
	}

	public void setTransmissionIds(final List<TransmissionId> transmissionIds)
	{
		this.transmissionIds = transmissionIds;
	}
}
