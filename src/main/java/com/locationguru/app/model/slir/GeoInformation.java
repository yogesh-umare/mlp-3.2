package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.GEO_INFO_TAG)
public class GeoInformation
{
	@XmlElement(name =  MLPConstants.COORDINATE_REFERENCE_SYSTEM)
	private CoordinateReferenceSystem coordinateReferenceSystem;

	public CoordinateReferenceSystem getCoordinateReferenceSystem()
	{
		return coordinateReferenceSystem;
	}

	public void setCoordinateReferenceSystem(final CoordinateReferenceSystem coordinateReferenceSystem)
	{
		this.coordinateReferenceSystem = coordinateReferenceSystem;
	}
}
