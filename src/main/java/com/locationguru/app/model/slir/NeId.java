package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.NEID_TAG)
public class NeId
{
	@XmlElement(name =  MLPConstants.VLRID_TAG)
	private VLRequestId vlRequestId;

	@XmlElement(name =  MLPConstants.VMSCID_TAG)
	private VmsCellId vmsCellID;

	public VLRequestId getVlRequestId()
	{
		return vlRequestId;
	}

	public void setVlRequestId(final VLRequestId vlRequestId)
	{
		this.vlRequestId = vlRequestId;
	}

	public VmsCellId getVmsCellID()
	{
		return vmsCellID;
	}

	public void setVmsCellID(final VmsCellId vmsCellID)
	{
		this.vmsCellID = vmsCellID;
	}
}
