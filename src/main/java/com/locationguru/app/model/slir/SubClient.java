package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import com.locationguru.app.enumerations.Enums;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.SUBCLIENT_TAG)
public class SubClient
{
	private String id;

	@XmlElement(name =  MLPConstants.PASSWORD_TAG)
	private String password;

	@XmlElement(name =  MLPConstants.SERVICE_ID_TAG)
	private String serviceId;

	@XmlAttribute(name =  MLPConstants.LAST_CLIENT_TAG)
	private Enums.Choice lastClient = Enums.Choice.NO;

	public Enums.Choice getLastClient()
	{
		return lastClient;
	}

	public void setLastClient(final Enums.Choice lastClient)
	{
		this.lastClient = lastClient;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public String getServiceId()
	{
		return serviceId;
	}

	public void setServiceId(final String serviceId)
	{
		this.serviceId = serviceId;
	}
}
