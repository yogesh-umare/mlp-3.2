package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.START_MSID_TAG)
public class StartMsid
{
	private Msid msid;

	public Msid getMsid()
	{
		return msid;
	}

	public void setMsid(final Msid msid)
	{
		this.msid = msid;
	}
}
