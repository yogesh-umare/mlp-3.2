package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.QUALITY_OF_POSITION_TAG)
public class QualityOfPosition
{
	@XmlElement(name =  MLPConstants.MAX_LOCATION_AGE_TAG)
	private String maxLocationAge;

	@XmlElement(name =  MLPConstants.RESPONSE_REQUIRED_TAG)
	private ResponseRequired responseRequired;

	@XmlElement(name =  MLPConstants.HORIZONTAL_ACCURACY_TAG)
	private HorizontalAccuracy horizontalAccuracy;

	@XmlElement(name =  MLPConstants.RESPONSE_TIMER_TAG)
	private String responseTimer;

	@XmlElement(name =  MLPConstants.LATLONG_ACCURACY_TAG)
	private LatlongAccuracy latlongAccuracyVO;

	@XmlElement(name =  MLPConstants.ALTITUDE_ACCURACY_TAG)
	private AltitudeAccuracy altitudeAccuracyVO;

	public String getMaxLocationAge()
	{
		return maxLocationAge;
	}

	public void setMaxLocationAge(String maxLocationAge)
	{
		this.maxLocationAge = maxLocationAge;
	}

	public ResponseRequired getResponseRequired()
	{
		return responseRequired;
	}

	public HorizontalAccuracy getHorizontalAccuracy()
	{
		return horizontalAccuracy;
	}

	public void setHorizontalAccuracy(final HorizontalAccuracy horizontalAccuracy)
	{
		this.horizontalAccuracy = horizontalAccuracy;
	}

	public String getResponseTimer()
	{
		return responseTimer;
	}

	public void setResponseTimer(final String responseTimer)
	{
		this.responseTimer = responseTimer;
	}

	public LatlongAccuracy getLatlongAccuracyVO()
	{
		return latlongAccuracyVO;
	}

	public void setLatlongAccuracyVO(final LatlongAccuracy latlongAccuracyVO)
	{
		this.latlongAccuracyVO = latlongAccuracyVO;
	}

	public AltitudeAccuracy getAltitudeAccuracyVO()
	{
		return altitudeAccuracyVO;
	}

	public void setAltitudeAccuracyVO(final AltitudeAccuracy altitudeAccuracyVO)
	{
		this.altitudeAccuracyVO = altitudeAccuracyVO;
	}
}
