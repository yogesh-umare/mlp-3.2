package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.MSID_RANGE_TAG)
public class MsidRange
{
	@XmlElement(name =  MLPConstants.START_MSID_TAG)
	private StartMsid startMsid;

	@XmlElement(name =  MLPConstants.STOP_MSID_TAG)
	private StopMsid stopMsid;

	public StartMsid getStartMsid()
	{
		return startMsid;
	}

	public void setStartMsid(final StartMsid startMsid)
	{
		this.startMsid = startMsid;
	}

	public StopMsid getStopMsid()
	{
		return stopMsid;
	}

	public void setStopMsid(final StopMsid stopMsid)
	{
		this.stopMsid = stopMsid;
	}
}
