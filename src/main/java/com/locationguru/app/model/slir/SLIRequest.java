package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import com.locationguru.app.enumerations.Enums;
import com.locationguru.app.model.common.RequestVersion;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.SLIREQUEST_TAG)
public class SLIRequest extends RequestVersion
{
	@XmlAttribute(name =  MLPConstants.RESPONSE_TYPE_ATTRIBUTE)
	private Enums.ResponseType responseType = Enums.ResponseType.SYNC;

	@XmlElement(name =  MLPConstants.MSIDS_TAG)
	private Msids msids;

	@XmlElement(name =  MLPConstants.MSID_TAG)
	private List<Msid> msidList = new ArrayList<>();

	@XmlElement(name =  MLPConstants.CODEWORD_TAG)
	private List<Codeword> codewords;

	@XmlElement(name =  MLPConstants.GSM_NETPRARAM_TAG)
	private Set<GsmNetworkParameter> gsmNetworkParameters = new HashSet<>();

	@XmlElement(name =  MLPConstants.TRANS_ID_TAG)
	private List<TransmissionId> transmissionIds;

	@XmlElement(name =  MLPConstants.QUALITY_OF_POSITION_TAG)
	private QualityOfPosition qualityOfPosition;

	@XmlElement(name =  MLPConstants.GEO_INFO_TAG)
	private GeoInformation geoInformation;

	@XmlElement(name =  MLPConstants.LOCATION_TYPE_TAG)
	private LocationType locationType;

	@XmlElement(name =  MLPConstants.PRIORITY_TAG)
	private Priority priority;

	@XmlElement(name =  MLPConstants.PUSH_ADDRESS_TAG)
	private PushAddress pushAddress;

	@XmlElement(name =  MLPConstants.SERVICE_COVERAGE_TAG)
	private ServiceCoverage serviceCoverageDTO;

	public List<Msid> getMsidList()
	{
		return msidList;
	}

	public void setMsidList(List<Msid> msidList)
	{
		this.msidList = msidList;
	}

	public QualityOfPosition getQualityOfPosition()
	{
		return qualityOfPosition;
	}

	public void setQualityOfPosition(QualityOfPosition qualityOfPosition)
	{
		this.qualityOfPosition = qualityOfPosition;
	}

	public LocationType getLocationType()
	{
		return locationType;
	}

	public Priority getPriority()
	{
		return priority;
	}

	public Enums.ResponseType getResponseType()
	{
			return responseType;
	}

	public PushAddress getPushAddress()
	{
		return pushAddress;
	}

	public void setPushAddress(PushAddress pushAddress)
	{
		this.pushAddress = pushAddress;
	}

	public Msids getMsids()
	{
		return msids;
	}

	public void setMsids(final Msids msids)
	{
		this.msids = msids;
	}

	public List<Codeword> getCodewords()
	{
		return codewords;
	}

	public void setCodewords(final List<Codeword> codewords)
	{
		this.codewords = codewords;
	}

	public Set<GsmNetworkParameter> getGsmNetworkParameters()
	{
		return gsmNetworkParameters;
	}

	public void setGsmNetworkParameters(final Set<GsmNetworkParameter> gsmNetworkParameters)
	{
		this.gsmNetworkParameters = gsmNetworkParameters;
	}

	public List<TransmissionId> getTransmissionIds()
	{
		return transmissionIds;
	}

	public void setTransmissionIds(final List<TransmissionId> transmissionIds)
	{
		this.transmissionIds = transmissionIds;
	}

	public GeoInformation getGeoInformation()
	{
		return geoInformation;
	}

	public void setGeoInformation(final GeoInformation geoInformation)
	{
		this.geoInformation = geoInformation;
	}

	public ServiceCoverage getServiceCoverageDTO()
	{
		return serviceCoverageDTO;
	}

	public void setServiceCoverageDTO(final ServiceCoverage serviceCoverageDTO)
	{
		this.serviceCoverageDTO = serviceCoverageDTO;
	}
}
