package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.GSM_NETPRARAM_TAG)
public class GsmNetworkParameter
{
	@XmlElement(name =  MLPConstants.CGI_TAG)
	private CellGlobalIdentity cellGlobalIdentity;

	@XmlElement(name =  MLPConstants.NEID_TAG)
	private NeId neId;

	@XmlElement(name =  MLPConstants.NMR_TAG)
	private String networkManagementReport;

	@XmlElement(name =  MLPConstants.TA_TAG)
	private String timingAdvance;

	@XmlElement(name =  MLPConstants.LMSI_TAG)
	private String localMobileStationId;

	@XmlElement(name =  MLPConstants.IMSI_TAG)
	private String internationMobileStationId;

	public CellGlobalIdentity getCellGlobalIdentity()
	{
		return cellGlobalIdentity;
	}

	public void setCellGlobalIdentity(final CellGlobalIdentity cellGlobalIdentity)
	{
		this.cellGlobalIdentity = cellGlobalIdentity;
	}

	public NeId getNeId()
	{
		return neId;
	}

	public void setNeId(final NeId neId)
	{
		this.neId = neId;
	}

	public String getNetworkManagementReport()
	{
		return networkManagementReport;
	}

	public void setNetworkManagementReport(final String networkManagementReport)
	{
		this.networkManagementReport = networkManagementReport;
	}

	public String getTimingAdvance()
	{
		return timingAdvance;
	}

	public void setTimingAdvance(final String timingAdvance)
	{
		this.timingAdvance = timingAdvance;
	}

	public String getLocalMobileStationId()
	{
		return localMobileStationId;
	}

	public void setLocalMobileStationId(final String localMobileStationId)
	{
		this.localMobileStationId = localMobileStationId;
	}

	public String getInternationMobileStationId()
	{
		return internationMobileStationId;
	}

	public void setInternationMobileStationId(final String internationMobileStationId)
	{
		this.internationMobileStationId = internationMobileStationId;
	}
}
