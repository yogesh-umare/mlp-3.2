package com.locationguru.app.model.slir;

import com.locationguru.app.enumerations.Enums.SessionType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class Session
{
	@XmlAttribute
	private SessionType type;

	@XmlValue
	private String value;

	public SessionType getType()
	{
		return type;
	}

	public void setType(final SessionType type)
	{
		this.type = type;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}
}
