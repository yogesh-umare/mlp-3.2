package com.locationguru.app.model.slir;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.IDENTIFIER)
public class Identifier
{
	private String code;

	private String codeSpace;

	private String edition;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getCodeSpace()
	{
		return codeSpace;
	}

	public void setCodeSpace(final String codeSpace)
	{
		this.codeSpace = codeSpace;
	}

	public String getEdition()
	{
		return edition;
	}

	public void setEdition(final String edition)
	{
		this.edition = edition;
	}

}
