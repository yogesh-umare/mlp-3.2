package com.locationguru.app.model.common;

import com.locationguru.app.enumerations.Enums.RequestModeConstant;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class RequestMode
{
	@XmlAttribute
	RequestModeConstant type = RequestModeConstant.PASSIVE;

	public RequestModeConstant getType()
	{
		return type;
	}
}