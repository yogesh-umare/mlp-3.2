package com.locationguru.app.model.common;

import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.CLIENT_TAG)
public class Client
{
	@XmlElement(name =  MLPConstants.REQUEST_MODE_TAG)
	RequestMode requestMode;

	private String id;

	@XmlElement(name =  MLPConstants.PASSWORD_TAG)
	private String password;

	@XmlElement(name =  MLPConstants.SERVICE_ID_TAG)
	private String serviceId;

	public RequestMode getRequestMode()
	{
		return requestMode;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public String getServiceId()
	{
		return serviceId;
	}

	public void setServiceId(final String serviceId)
	{
		this.serviceId = serviceId;
	}
}
