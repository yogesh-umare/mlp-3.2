package com.locationguru.app.model.common;

import com.locationguru.app.utils.MLPConstants;
import com.locationguru.app.enumerations.Enums.RequestorType;
import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Requestor
{
	@XmlAttribute
	private RequestorType type = RequestorType.MSISDN;

	@XmlElement(required = true)
	private String id;

	@XmlElement(name =  MLPConstants.SERVICE_ID_TAG)
	private String serviceId;

	public RequestorType getType()
	{
			return type;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getServiceId()
	{
		return serviceId;
	}

	public void setServiceId(final String serviceId)
	{
		this.serviceId = serviceId;
	}
}