package com.locationguru.app.model.common;

import com.locationguru.app.model.slir.SubClient;
import com.locationguru.app.utils.MLPConstants;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = MLPConstants.HEADER_TAG)
public class Header extends RequestVersion
{
	@XmlElement
	private Client client;

	@XmlElement
	private Requestor requestor;

	@XmlElement(name =  MLPConstants.SESSION_ID_TAG)
	private String sessionId;

	@XmlElement(name =  MLPConstants.SUBCLIENT_TAG)
	private List<SubClient> subClient = new ArrayList<>();

	public Client getClient()
	{
		return client;
	}

	public void setClient(final Client client)
	{
		this.client = client;
	}

	public Requestor getRequestor()
	{
		return requestor;
	}

	public void setRequestor(final Requestor requestor)
	{
		this.requestor = requestor;
	}

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

	public List<SubClient> getSubClient()
	{
		return subClient;
	}

	public void setSubClient(final List<SubClient> subClient)
	{
		this.subClient = subClient;
	}
}
