package com.locationguru.app.model.common;

import com.locationguru.app.utils.MLPConstants;
import org.springframework.stereotype.Service;
import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;

@Service
public class RequestVersion implements Serializable
{
	@XmlAttribute(name =  MLPConstants.VERSION_ATTRIBUTE)
	String version = MLPConstants.DEFAULT_VERSION;

	public String getVersion()
	{
		return version;
	}
}
