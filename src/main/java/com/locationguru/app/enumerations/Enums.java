package com.locationguru.app.enumerations;

/**
 * Project : SmartMiddleware
 * User: Rahul Raja
 * Date: 10 - 04 - 2013, 6:34 PM
 * Company : LocationGuru Solutions
 */
public class Enums
{
	/**
	 * Specifies the event that initiated the positioning
	 */
	public enum ChangeAreaType
	{
		MS_ENTERING, MS_LEAVING, MS_WITHIN_AREA
	}

	/**
	 * The positioning method used to obtain the associated location estimate
	 */
	public enum PositioningMethod
	{
		CELL("CELL"), OTDOA("OTDOA"), GPS("GPS"), AGPS("A-GPS"), EOTD("E-OTD"), UTDOA("U-TDOA"),
		AFLT("AFLT"), EFLT("EFLT"), UNKNOWN("UNKNOWN"), OTHER("OTHER");

		public String value;

		private PositioningMethod(String value)
		{
			this.value = value;
		}
	}

	/**
	 * Defines the type of location requested
	 */
	public enum LocationTypeConstant
	{
		CURRENT, LAST, LAST_OR_CURRENT, CURRENT_OR_LAST;
	}

	public enum Encoding
	{
		ASC,CRP;
	}

	/**
	 * Type of identifier for the mobile subscriber
	 */
	public enum MSIDType
	{
		MSISDN, IMSI;

		public static String getValue(int code)
		{
			String value = "";

			switch (code)
			{
				case 1:
					value = MSISDN.toString();
					break;
				case 2:
					value = IMSI.toString();
					break;
				default:
					value = MSISDN.toString();
					break;
			}
			return value;
		}
	}

	public enum RequestorType
	{
		MSISDN;
	}


	/**
	 * Priority of the location request
	 */
	public enum PriorityConstant
	{

		NORMAL, HIGH
	}

	/**
	 * Response time required
	 */
	public enum ResponseTimeType
	{
		NO_DELAY, LOW_DELAY, DELAY_TOL
	}

	public enum QosClass
	{
		BEST_EFFORT, ASSURED;
	}


	/**
	 * The type of the service that has been requested
	 */
	public enum RequestModeConstant
	{
		PASSIVE;

		/*private int value;
		private String name;

		RequestModeConstant(final int value, final String name)
		{
			this.value = value;
			this.name = name;
		}

		public String getName(){
			return name;
		}*/
	}

	/**
	 * Specifies the trigger that initiated the positioning of the MS at a triggered location report.
	 */
	public enum TRLTrigger
	{
		PERIODIC, MS_AVAIL, CHANGE_AREA
	}

	/**
	 * Defines a response type at the Standard Location and Emergency Immediate Service.
	 */
	public enum ResponseType
	{
		SYNC, ASYNC
	}

	public enum Choice
	{
		YES,NO;
	}

	public enum SessionType
	{
		APN,DIAL;
	}


	public enum ErrorResponse
	{
		OK(0, "OK"), SYSTEM_FAILURE(1, "SYSTEM FAILURE"), UNSPECIFIED_ERROR(2, "UNSPECIFIED ERROR"), UNKNOWN_SUBSCRIBER(4, "UNKNOWN SUBSCRIBER"), ABSENT_SUBSCRIBER(5, "ABSENT SUBSCRIBER"),
		CONGESTION_IN_LOCATION_SERVER(101, "CONGESTION IN LOCATION SERVER"), CONGESTION_IN_MOBILE_NETWORK(102, "CONGESTION IN MOBILE NETWORK"), DISALLOWED_BY_LOCAL_REGULATIONS(204, "DISALLOWED BY LOCAL REGULATIONS"),
		MISCONFIGURATION_OF_LOCATION_SERVER(207, "MISCONFIGURATION OF LOCATION SERVER"), SS7_OPTIMISATION(500, "SS7 OPTIMISATION"), REMOTE_FACILITY_NOT_SUPPORTED(502, "REMOTE FACILITY NOT SUPPORTED"), SERVICE_NOT_PROVISIONED(504, "SERVICE NOT PROVISIONED"),
		SMS_DELIVERY_FAILURE(520, "SMS DELIVERY FAILURE"), SUBSCRIBER_BUSY_FOR_MT_SMS(521, "SUBSCRIBER BUSY FOR MT SMS"), SM_MEMORY_CAPABILITY_EXCEEDED(522, "SM MEMORY CAPABILITY EXCEEDED"),
		INVALID_PROTOCOL_ELEMENT_ATTRIBUTE_VALUE(111,"INVALID PROTOCOL ELEMENT ATTRIBUTE VALUE");

		public int code;
		public String msg;

		private ErrorResponse(int code, String msg)
		{
			this.code = code;
			this.msg = msg;
		}


		public static String getErrorMessage(int i)
		{
			ErrorResponse response = SYSTEM_FAILURE;
			switch (i)
			{
				case 0:
					response = OK;
					break;
				case 1:
					response = SYSTEM_FAILURE;
					break;
				case 2:
					response = UNSPECIFIED_ERROR;
					break;
				case 4:
					response = UNKNOWN_SUBSCRIBER;
					break;
				case 5:
					response = ABSENT_SUBSCRIBER;
					break;
				case 101:
					response = CONGESTION_IN_LOCATION_SERVER;
					break;
				case 102:
					response = CONGESTION_IN_MOBILE_NETWORK;
					break;
				case 204:
					response = DISALLOWED_BY_LOCAL_REGULATIONS;
					break;
				case 207:
					response = MISCONFIGURATION_OF_LOCATION_SERVER;
					break;
				case 500:
					response = SS7_OPTIMISATION;
					break;
				case 502:
					response = REMOTE_FACILITY_NOT_SUPPORTED;
					break;
				case 504:
					response = SERVICE_NOT_PROVISIONED;
					break;
				case 520:
					response = SMS_DELIVERY_FAILURE;
					break;
				case 521:
					response = SUBSCRIBER_BUSY_FOR_MT_SMS;
					break;
				case 522:
					response = SM_MEMORY_CAPABILITY_EXCEEDED;
					break;
				case 111:
					response = INVALID_PROTOCOL_ELEMENT_ATTRIBUTE_VALUE;
					break;
			}
			return response.msg;
		}
	}
}
