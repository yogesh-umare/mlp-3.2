package com.locationguru.app.enumerations;


public enum MlpStatusCodes
{
	INVALID_ELEMENT_ATTRIBUTE_VALUE(111,"INVALID PROTOCOL ELEMENT ATTRIBUTE VALUE"),
	UNSUPPORTED_VERSION(103,"UNSUPPORTED VERSION"),
	FORMAT_ERROR(105,"FORMAT ERROR"),
	INVALID_ELEMENT_VALUE(110,"INVALID PROTOCOL ELEMENT VALUE"),
	ATTRIBUTE_VALUE_NOT_SUPPORTED(113,"PROTOCOL ELEMENT ATTRIBUTE VALUE NOT SUPPORTED");

	MlpStatusCodes(Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	private final Integer value;

	private final String name;

	public Integer getValue() {
		return value;
	}

	public String getName() {
		return name;
	}


}
