package com.locationguru.app.controller;

import com.locationguru.app.model.init.ServiceInit;
import com.locationguru.app.service.SerializationSevice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
@RequestMapping("/input-data")
public class SLIServiceController
{
	private static final Logger logger = LogManager.getLogger(SLIServiceController.class);

	private final SerializationSevice serializationSevice;

	@Autowired
	public SLIServiceController(final SerializationSevice serializationSevice)
	{
		this.serializationSevice = serializationSevice;
	}

	@PostMapping("/xml-request")
	public ResponseEntity acceptInput(@Valid @RequestBody String data)
	{
		ServiceInit serviceInit = serializationSevice.deserialize(data);
		return ResponseEntity.status(HttpStatus.OK).body(serviceInit);
	}

}
