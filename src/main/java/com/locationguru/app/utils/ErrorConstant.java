package com.locationguru.app.utils;

public class ErrorConstant
{
	public static final String SESSION_TYPE_REQUIRED = "Must need to specify type as APN or DIAL";
	public static final String MLP_VERSION = "Request version must be 3.2.0";

}
