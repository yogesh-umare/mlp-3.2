package com.locationguru.app.utils;


public class MLPConstants
{
	// SLIS XML Request Parameter constants
	public static final String SERVICE_INIT_TAG = "svc_init";
	public static final String HEADER_TAG = "hdr";
	public static final String CLIENT_TAG = "client";
	public static final String ID_TAG = "id";
	public static final String PASSWORD_TAG = "pwd";
	public static final String REQUESTOR_TAG = "requestor";
	public static final String SLIREQUEST_TAG = "slir";
	public static final String MSIDS_TAG = "msids";
	public static final String MSID_TAG = "msid";
	public static final String QUALITY_OF_POSITION_TAG = "eqop";
	public static final String MAX_LOCATION_AGE_TAG = "max_loc_age";
	public static final String RESPONSE_REQUIRED_TAG = "resp_req";
	public static final String HORIZONTAL_ACCURACY_TAG = "hor_acc";
	public static final String LATLONG_ACCURACY_TAG = "ll_acc";
	public static final String ALTITUDE_ACCURACY_TAG = "alt_acc";
	public static final String LOCATION_TYPE_TAG = "loc_type";
	public static final String PRIORITY_TAG = "prio";
    public static final String RESPONSE_TYPE_ATTRIBUTE = "res_type";
    public static final String TRANS_ID_TAG = "trans_id";
	public static final String SERVICE_ID_TAG = "serviceid";
	public static final String REQUEST_MODE_TAG = "requestmode";
	public static final String SESSION_ID_TAG = "sessionid";
	public static final String SUBCLIENT_TAG = "subclient";
	public static final String LAST_CLIENT_TAG = "last_client";
	public static final String MSID_RANGE_TAG = "msid_range";
	public static final String START_MSID_TAG = "start_msid";
	public static final String STOP_MSID_TAG = "stop_msid";
	public static final String CODEWORD_TAG = "codeword";
	public static final String RESPONSE_TIMER_TAG = "resp_timer";
	public static final String SERVICE_COVERAGE_TAG = "service_coverage";
	public static final String ENCODING = "enc";
	public static final String VLRID_TAG = "vlrid";
	public static final String TA_TAG = "ta";
	public static final String LMSI_TAG = "lmsi";
	public static final String IMSI_TAG = "imsi";
	public static final String NMR_TAG = "nmr";


	// SLIS XML Response Parameter constants
	public static final String SERVICE_RESULT = "svc_result";
	public static final String SLIRESPONSE_TAG = "slia";
	public static final String SLIREPORT_TAG = "slirep";
	public static final String POSITION_TAG = "pos";
	public static final String POSITION_METHOD_ATTRIBUTE = "pos_method";
	public static final String MSID_RESPONSE_TAG = "msid";
	public static final String POINT_DETAILS_TAG = "pd";
	public static final String TIME_TAG = "time";
	public static final String SHAPE_TAG = "shape";
	public static final String COORDINATE_TAG = "coord";
	public static final String X_AXIS_TAG = "X";
	public static final String Y_AXIS_TAG = "Y";
	public static final String RADIUS_TAG = "radius";
	public static final String POSITION_ERROR_TAG = "poserr";
	public static final String ERROR_RESULT_TAG = "result";
	public static final String ERROR_RESULT_ID = "resid";
	public static final String CIRCULAR_AREA_TAG = "CircularArea";
	public static final String POINT_TAG = "Point";
	public static final String POLYGON_TAG = "Polygon";
	public static final String CIRCULAR_ARC_AREA_TAG = "CircularArcArea";
	public static final String ELLIPTICAL_AREA_TAG = "EllipticalArea";
	public static final String SERVICE_RESULT_TAG = "<svc_result";
	public static final String VERSION_ATTRIBUTE = "ver";
	public static final String MSID_TYPE_ATTRIBUTE = "type";
	public static final String REQ_ID_TAG = "req_id";
    public static final String MA_SLIRESPONSE_TAG = "ma:slia-extension";
    public static final String MA_NETWORK_TAG = "ma:network-parameters";
    public static final String MSID_REF_TAG = "msid-ref";
    public static final String MSID_SET_TAG = "ma:msid-set";
    public static final String REF_TYPE_TAG = "ref-type";
    public static final String REF_VALUE_TAG = "ref-value";
    public static final String MA_MSID_TAG =  "ma:msid";
    public static final String MA_LTE_NET_PARAM_TAG = "ma:lte-net-param";
    public static final String TYPE_TAG = "type";
    public static final String VALUE_TAG = "value";

    public static final String TIMESTAMP_TAG = "timestamp";
    public static final String MCC_TAG = "mcc";
    public static final String MNC_TAG = "mnc";
    public static final String MA_LTE_CI_TAG = "ma:lte-ci";
    public static final String MA_MEID_TAG = "ma:mmeid";
    public static final String CC_TAG = "cc";
    public static final String MA_MME_NAME_TAG = "ma:mme-name";

    public static final String NEID_TAG = "neid";
    public static final String VMSCID_TAG = "vmscid";
    public static final String NDC_TAG = "ndc";
    public static final String VMSCNO_TAG = "vmscno";

    public static final String MA_SGSNID_TAG = "ma:sgsnid";
    public static final String MA_SGSNNO_TAG = "ma:sgsnno";

    public static final String GSM_NETPRARAM_TAG = "gsm_net_param";
    public static final String CGI_TAG = "cgi";
    public static final String LAC_TAG = "lac";
    public static final String CELLID_TAG = "cellid";
    public static final String MA_WCDMA_NETPARAM_TAG = "ma:wcdma-net-param";
    public static final String MA_CELLID_MESRESULT_TAG = "ma:cellid-mes-result";
    public static final String MA_GLOBAL_UCID_TAG = "ma:global-uc-id";
    public static final String MA_UCID_TAG = "ma:uc-id";
    public static final String MA_RNCID_TAG = "ma:rnc-id";
    public static final String MA_CID_TAG = "ma:c-id";
    public static final String MA_UT_ACCESS_POINT_TAG = "ma:utran-access-point-pos";
    public static final String MA_ALT_TAG = "alt";
    public static final String MA_RTT_INFO_TAG = "ma:rtt-info";
    public static final String MA_RTT_TAG = "ma:rtt";
    public static final String MA_UE_RX_TIME_DIFF_TAG = "ma:ue-rx-tx-time-diff2";
    public static final String MA_UE_POS_QUALITY_TAG = "ma:ue-pos-measurement-quality";

    public static final String MA_STD_RESOLUTION_TAG = "ma:std-resolution";
    public static final String MA_N_MEASUREMENT_TAG = "ma:n-measurements";
    public static final String MA_STD_OF_MEASUREMENT_TAG = "ma:std-of-measurements";

	// TLRS XML request constants
	public static final String TLRREQUEST_TAG = "tlrr";
	public static final String STOP_TIME_TAG = "stop_time";
	public static final String START_TIME_TAG = "start_time";
	public static final String UTC_OFFSET_ATTRIBUTE = "utc_off";
	public static final String PUSH_ADDRESS_TAG = "pushaddr";
	public static final String PUSH_ADDRESS_URL_TAG = "url";
	public static final String PUSH_ADDRESS_USER_ID = "id";
	public static final String PUSH_ADDRESS_PASSWORD = "pwd";
	public static final String INTERVAL_TAG = "interval";
	public static final String TLR_REPORT_TAG = "tlrep";
	public static final String QOP_TAG = "qop";
	public static final String QOS_CLASS_ATTR = "qos_class";
	public static final String TLRR_EVENT_TAG = "tlrr_event";
	public static final String MS_ACTION_TAG = "ms_action";
	public static final String CHANGE_AREA_TAG = "change_area";
	public static final String TARGET_AREA = "target_area";
	public static final String NAME_AREA = "name_area";
	public static final String COORDINATE_REFERENCE_SYSTEM = "CoordinateReferenceSystem";
	public static final String IDENTIFIER = "Identifier";
	public static final String GEO_INFO_TAG = "geo_info";
	public static final String DURATION_TAG = "duration";
	public static final String DEFAULT_LOCATION_ESTIMATES = "FALSE";
	public static final String TLR_STOP_REQUEST_TAG = "tlrsr";
	public static final String TLR_STOP_ANSWER_TAG = "tlrsa";


	// TLRA XML constants
	public static final String TLRANSWER_TAG = "tlra";
	public static final String REQUEST_ID_TAG = "req_id";
	public static final String TRL_POSITION_TAG = "trl_pos";
	public static final String TRL_TRIGGER_ATTRIBUTE = "trl_trigger";
	public static final String TIME_REMAINING_TAG = "time_remaining";

	// Other constant values
	public static final String DEFAULT_VERSION = "3.2.0";
	public static final String REQUEST_MODE = "PASSIVE";
	public static final String DEFAULT_HORIZONTAL_ACCURACY = "1000";
	public static final String DEFAULT_TIME_OFFSET_VALUE = "+0530";
	public static final String DEFAULT_DATE_FORMAT = "yyyyMMddHHmmss";
	public static final Integer MSID_SIZE = 20;
	public static final String INITIAL = "INITIAL";


}
