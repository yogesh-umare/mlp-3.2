package com.locationguru.app.utils;


import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "result")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class ErrorResponse implements Serializable
{
	@XmlElement
	private String message;

	@XmlElement
	private Integer code;

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	public Integer getCode()
	{
		return code;
	}

	public void setCode(final Integer code)
	{
		this.code = code;
	}

	public ErrorResponse(Integer code, String message)
	{
		super();
		this.code = code;
		this.message = message;

	}

	public ErrorResponse()
	{
		super();
	}
}